package com.caka.libs.printer.services;

import java.util.Map;

/**
 * Created by Phung Dang Hoan on 2019/10/20
 */
public interface BluetoothServiceStateObserver {
    void onBluetoothServiceStateChanged(int state, Map<String,Object> boundle);
}
