package com.caka.libs.printer;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.caka.libs.printer.utils.DefinesPromise;
import com.caka.libs.printer.utils.StringUtils;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;

import net.posprinter.posprinterface.IMyBinder;
import net.posprinter.posprinterface.ProcessData;
import net.posprinter.posprinterface.UiExecute;
import net.posprinter.service.PosprinterService;
import net.posprinter.utils.BitmapToByteData;
import net.posprinter.utils.DataForSendToPrinterPos80;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phung Dang Hoan on 2019/10/20
 */
public class BluetoothPrinterModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private IMyBinder binder;

    private void initServices(final String addressDevice, final Promise promise) {
        ServiceConnection conn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                // Bind successfully
                binder = (IMyBinder) iBinder;
                Log.e("Caka", "Service connected");
                connect(addressDevice, promise);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.e("Caka", "Service Disconnected");
            }
        };
        Intent intent = new Intent(reactContext, PosprinterService.class);
        getReactApplicationContext().bindService(intent, conn, reactContext.BIND_AUTO_CREATE);
    }

    public BluetoothPrinterModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "BluetoothPrinter";
    }

    /**
     * show the massage
     *
     * @param showstring content
     */
    private void showToast(String showstring) {
        Toast.makeText(reactContext, showstring, Toast.LENGTH_SHORT).show();
    }


    @ReactMethod
    public void connectDevice(String addressDevice, final Promise promise) {
        if(this.binder == null){
            initServices(addressDevice, promise);
        }else {
            connect(addressDevice, promise);
        }
    }

    private void connect (String addressDevice, final Promise promise) {
        Log.e("Caka", "addressDevice: "+ addressDevice);
        if (addressDevice.equals(null) || addressDevice.equals("")) {
            showToast("Địa chỉ thiết bị không đúng định dạng");
        } else {
            this.binder.connectBtPort(addressDevice, new UiExecute() {
                @Override
                public void onsucess() {
                    showToast("Kết nối máy in thành công");
                    promise.resolve(null);
                }

                @Override
                public void onfailed() {
                    showToast("Kết nối với thiết bị thất bại");
                    promise.reject(DefinesPromise.CONNECT_DEVICE_FAILED.getStCode(), DefinesPromise.CONNECT_DEVICE_FAILED.getMessager());
                }
            });
        }
    }

    @ReactMethod
    public void printText(final String contentsText, @Nullable ReadableMap options, final Promise promise) {
        byte[] data1 = StringUtils.strTobytes(contentsText);
        if(this.binder == null){
            promise.reject(DefinesPromise.LOST_CONNECT_DEVICE.getStCode(), DefinesPromise.LOST_CONNECT_DEVICE.getMessager());
            return;
        }
        this.binder.write(data1, new UiExecute() {
            @Override
            public void onsucess() {
                Log.d("Caka", "Print-Text Success: " + contentsText);
                promise.resolve(null);
            }

            @Override
            public void onfailed(){
                Log.e("Caka", "Print-Text Failed: " + contentsText);
                promise.reject(DefinesPromise.COMMAND_COULD_NOT_PERFORMED.getStCode(), DefinesPromise.LOST_CONNECT_DEVICE.getMessager());
            }
        });
    }

    @ReactMethod
    public void printBarCode(final String contentBar, @Nullable ReadableMap options, final Promise promise ) {
        if(this.binder == null){
            promise.reject(DefinesPromise.LOST_CONNECT_DEVICE.getStCode(), DefinesPromise.LOST_CONNECT_DEVICE.getMessager());
            return;
        }
        int width = 0;
        int height = 0;
        int length = 0;
        int position = 0;
        int alignment = 0;
        if (options != null) {
            width = options.hasKey("width") ? options.getInt("width") : 56;
            height = options.hasKey("height") ? options.getInt("height") : 110;
            position = options.hasKey("position") ? options.getInt("position") : 02;
            alignment = options.hasKey("alignment") ? options.getInt("alignment") : 1;
        }
        if(contentBar != null) {
            length= contentBar.length();
        }

        final int finalLength = length;
        final int finalWidth = width;
        final int finalHeight = height;
        final int finalPosition = position;
        final int finalAlignment = alignment;
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
                Log.e("Caka", "Print-Barcode Success: ");
                promise.resolve(null);
            }

            @Override
            public void onfailed() {
                Log.e("Caka", "Print-Barcode Failed: ");
                promise.reject(DefinesPromise.COMMAND_COULD_NOT_PERFORMED.getStCode(), DefinesPromise.LOST_CONNECT_DEVICE.getMessager());
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list = new ArrayList<byte[]>();
                // initialize the printer
                list.add(DataForSendToPrinterPos80.initializePrinter());
                //     // select alignment
                list.add(DataForSendToPrinterPos80.selectAlignment(finalAlignment));
                //     // select HRI position
                list.add(DataForSendToPrinterPos80.selectHRICharacterPrintPosition(finalPosition));
                //     // set the width
//                     list.add(DataForSendToPrinterPos80.setBarcodeWidth(finalWidth));
                //     // set the height ,usually 162
                list.add(DataForSendToPrinterPos80.setBarcodeHeight(finalHeight));
                // print barcode ,attention,there are two method for barcode.
                // different barcode type,please refer to the programming manual
                // UPC-A
                list.add(DataForSendToPrinterPos80.printBarcode(73, finalLength, contentBar));

                list.add(DataForSendToPrinterPos80.printAndFeedLine());

                return list;
            }
        });
    }

    @ReactMethod
    public void printPicture(final String strBase64,  @Nullable ReadableMap options, final Promise promise ) {
        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
        final Bitmap printBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
                promise.resolve(null);
            }

            @Override
            public void onfailed() {
                Log.e("Caka", "Print-Picture Failed: ");
                promise.reject(DefinesPromise.COMMAND_COULD_NOT_PERFORMED.getStCode(), DefinesPromise.LOST_CONNECT_DEVICE.getMessager());
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list=new ArrayList<byte[]>();
                list.add(DataForSendToPrinterPos80.initializePrinter());
                list.add(DataForSendToPrinterPos80.printRasterBmp(
                        0,printBmp, BitmapToByteData.BmpType.Threshold, BitmapToByteData.AlignType.Left,576));
//                list.add(DataForSendToPrinterPos80.printAndFeedForward(3));
                list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66,1));
                return list;
            }
        });
    }
}
