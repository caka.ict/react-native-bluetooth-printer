package com.caka.libs.printer.utils;

public enum DefinesPromise {

    CONNECT_DEVICE_SUCCESS(200, "CONNECT_DEVICE_SUCCESS", "Kết nối máy in thành công."),
    LOST_CONNECT_DEVICE(500, "LOST_CONNECT_DEVICE", "Mất kết nối với máy in."),
    CONNECT_DEVICE_FAILED(501, "CONNECT_DEVICE_FAILED", "Kết nối máy in thất bại."),
    COMMAND_COULD_NOT_PERFORMED(502, "COMMAND_COULD_NOT_PERFORMED", "Lệnh in không thể thực hiện được.");


    private int code;
    private String stCode;
    private String messager;

    private DefinesPromise(int code, String stCode , String messager) {
        this.code = code ;
        this.messager = messager ;
        this.stCode = stCode ;
    }

    public int getCode() {
        return code;
    }

    public String getStCode() {
        return stCode;
    }

    public String getMessager() {
        return messager;
    }
}
